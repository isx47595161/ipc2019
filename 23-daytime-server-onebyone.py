# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
#   client                                         servidor
#-----------------------------------------------------------
#   1.S'engega                                     1.S'engega
#   2.Es connecta amb el servidor ---------------> 2.Escolta*
#   3.Escolta*  <---------------------|            3.Popen(date)
#   4.Print                           |----------- 4.For llegir [envia]
#   5.Tanca                                        5.Tanca
#----------------------------------------------------------------------
from subprocess import Popen, PIPE
import sys,socket,os,signal
ordre="date"
print(os.getpid())
HOST = ''
PORT = 50005
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    pipeData = Popen(ordre,stdout=PIPE)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()
sys.exit(0)
