# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql-multi.py
# *nota* no tracta el cas de num_clie inexistent
# -------------------------------------
# Adrià Quintero Lázaro
# @ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
#commandLocal = "psql -qtA -F ';' lab_clinic -c 'select * from pacients;'"
#commandRemote = "psql -qtA -h i11 -U postgres -F ';' training -c "select * from oficinas;"
# -------------------------------------------
import sys,os,signal
global comptaplus
global comptarest
comptaplus=0
comptarest=0

def myhandler(signum,frame):
    print("Signal handler called with signal: ", signum)
    print(f"Augmentat: {comptaplus}, Reduit: {comptarest}")
    sys.exit(1)

def mydeath(signum,frame):
    print("Signal handler called with signal: ", signum)
    print("No em dona la gana de morir :)")

def augmenta(signum,frame):
    global comptaplus
    print("Signal handler called with signal:",signum)
    print("Augmentant 60 segons")
    comptaplus+=1
    actual=signal.alarm(0)
    signal.alarm(actual+60)

def redueix(signum,frame):
    global comptarest
    print("Signal handler called with signal:",signum)
    print("Reduint 60 segons")
    comptarest+=1
    actual=signal.alarm(0)
    if actual-60<0:
        print("ignored %d" % (actual))
    else:
        signal.alarm(actual-60)

def reset(signum,frame):
    print("Signal handler called with signal:",signum)
    print("Restoring value: ", temps)
    signal.alarm(temps)

def segons(signum,frame):
    falta=signal.alarm(0)
    signal.alarm(falta)
    print("Falten actualment %d segons" % (falta))

temps=int(sys.argv[1])


signal.signal(signal.SIGALRM,myhandler)      #14
signal.signal(signal.SIGUSR2,redueix)        #12
signal.signal(signal.SIGUSR1,augmenta)       #10
signal.signal(signal.SIGTERM,segons)         #15
signal.signal(signal.SIGHUP,reset)
signal.signal(signal.SIGINT,mydeath)
signal.alarm(temps)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
