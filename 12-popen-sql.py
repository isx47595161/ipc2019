# /usr/bin/python
#-*- coding: utf-8-*-
#
# 12-popen-sql.py
# Traducció d'un "select * from clientes"
# ---------------------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# ---------------------------------------------------
from subprocess import Popen, PIPE
consulta = ["psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"select * from clientes;\""]
pipeData = Popen(consulta,stdout=PIPE, shell=True)
for line in pipeData.stdout:
        print(line.decode("utf-8"),end="")
exit(0)
