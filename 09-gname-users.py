#!/usr/bin/python3
# -*- coding: utf-8 -*-
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# 09-gname-users.py [-s login|gid|gname] -u fileusers -g filegroup
# Programa que crea usuaris i els mostra ordenats segons login,gid o gname
#--------------------------------------
#Imports i parsers
import sys, argparse
grupDic={}
parser = argparse.ArgumentParser(description=\
        """Llistar els usuaris de file o stdin (format /etc/passwd""",\
        epilog="thats all folks")
parser.add_argument("-s","--sort",type=str,help="métode d'ordenació",\
		dest="metode",metavar="metodesort",choices=["login","gid","gname"],default="gid")
parser.add_argument("-g","--grup",type=str,help="group file or stdin(/etc/group style)",\
        metavar="group_file", default="/dev/stdin",dest="fitxer_grups",required=True)
parser.add_argument("-u","--fit",type=str,\
        help="user file or stdin (/etc/passwd style)", metavar="file",\
        default="/dev/stdin",dest="fitxer", required=True)
args=parser.parse_args()
# -------------------------------------------------------
#Constructor
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if str(self.gid) in grupDic:
        self.gname=grupDic[str(self.gid)]
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gname: {self.gname} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
  def __str__(self):
    "Funció to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gname, self.gecos, self.home, self.shell)

class UnixGroup():
    """ClasseUnixUser: prototipus de /etc/group
    gname:passwd:gid"""
    def __init__(self,groupLine):
        "Constructor objectes UnixGroup"
        groupField=groupLine[:-1].split(":")
        self.gname=groupField[0]
        self.passwd=groupField[1]
        self.gid=groupField[2]
        self.userList=groupField[3]
    def show(self):
        "Mostra les dades de l'usuari"
        print(f"gname: {self.gname} passwd: {self.passwd} gid: {self.gid} userList: {self.userList}")

    def __str__(self):
        "Funció to string"
        return "%s %s %d %s" %(self.gname, self.passwd, self.gid, self.userList)
# -------------------------------------------------------
#Funcions
def cmp_login(user):
    """Funció que ordena segons el login"""
    return user.login

def cmp_gid(user):
    """Funció que ordena segons el login"""
    return int(user.gid)
def cmp_gname(user):
    """Funció que ordena segons el gname"""
    return user.gname
#--------------------------------------------------------
#Programa
#Fitxer grups
fileGroup=open(args.fitxer_grups,"r")
for line in fileGroup:
    grup=UnixGroup(line)
    grupDic[grup.gid]=grup.gname
fileGroup.close()
#Fitxer usuaris
fileIn=open(args.fitxer,"r")
userList=[]
for line in fileIn:
  oneUser=UnixUser(line)
  userList.append(oneUser)
fileIn.close()

if args.metode == "login":
	userList.sort(key=cmp_login)
elif args.metode == "gid":
	userList.sort(key=cmp_gid)
elif args.metode == "gname":
    userList.sort(key=cmp_gname)
for user in userList:
    user.show()
exit(0)
