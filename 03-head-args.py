#!/usr/bin/python3
# -*- coding: utf-8 -*-
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# 03-head-args.py [-n lines] [-f file]
# Aplicar al 01 aquestes dues opcions
# -------------------------------------
#Imports
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primeres línies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        metavar="numLines",default=10)
parser.add_argument("-f","--fit",type=str,\
        help="fitxer a processar", metavar="file",\
        default="/dev/stdin",dest="fitxer")
args=parser.parse_args()
print(args)
#----------------------------------------------------------
#Variables
MAXLIN = args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
    counter += 1
    print(line,end="")
    if counter == MAXLIN: break
fileIn.close()
exit(0)
################################################################################
#Solució

#import sys, argparse
#parser = argparse.ArgumentParser(description=\
#        """Mostrar les N primereslínies """,\
#        epilog="thats all folks")
#parser.add_argument("-n","--nlin",type=int,\
#        help="Número de línies",dest="nlin",\
#        metavar="numLines",default=10)
#parser.add_argument("-f","--fit",type=str,\
#        help="fitxer a processar", metavar="file",\
#        default="/dev/stdin",dest="fitxer")
#args=parser.parse_args()
#print(args)
# -------------------------------------------------------
#MAXLIN=args.nlin
#fileIn=open(args.fitxer,"r")
#counter=0
#for line in fileIn:
#  counter+=1
#  print(line, end=' ')
#  if counter==MAXLIN: break
#fileIn.close()
#exit(0)
