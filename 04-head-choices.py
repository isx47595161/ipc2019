#!/usr/bin/python3
# -*- coding: utf-8 -*-
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# 04-head-choices.py [-n5|10(default)|15] file
# Aplicar al 01 aquestes dues opcions
# -------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primereslínies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies 5|10|15, def 10",\
        dest="nlin",\
        metavar="numLines",default=10,\
        choices=[5,10,15])
parser.add_argument("fitxer",type=str,\
        help="fitxer a processar", metavar="file")
args=parser.parse_args()
print(args)
#----------------------------------------------------------
#Variables
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print (line, end=' ')
  if counter==MAXLIN: break
fileIn.close()
exit(0)
################################################################################
#import sys, argparse
#parser = argparse.ArgumentParser(description=\
#        """Mostrar les N primereslínies """,\
#        epilog="thats all folks")
#parser.add_argument("-n","--nlin",type=int,\
#        help="Número de línies 5|10|15, def 10",\
#        dest="nlin",\
#        metavar="numLines",default=10,\
#        choices=[5,10,15])
#parser.add_argument("fitxer",type=str,\
#        help="fitxer a processar", metavar="file")
#args=parser.parse_args()
#print(args)
# -------------------------------------------------------
#MAXLIN=args.nlin
#fileIn=open(args.fitxer,"r")
#counter=0
#for line in fileIn:
#  counter+=1
#  print (line, end=' ')
#  if counter==MAXLIN: break
#fileIn.close()
#exit(0)
