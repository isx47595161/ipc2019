# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
#   client                                         servidor
#-----------------------------------------------------------
#   1.S'engega                                     1.S'engega
#   2.Es connecta amb el servidor ---------------> 2.Escolta*
#   3.Escolta*  <---------------------|            3.Popen(date)
#   4.Print                           |----------- 4.For llegir [envia]
#   5.Tanca                                        5.Tanca
#----------------------------------------------------------------------
from subprocess import Popen, PIPE
import sys,socket
HOST = "35.176.54.105" #AWS
PORT = 50006
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
    data = s.recv(1024)
    if not data: break
    print('Data:',repr(data))
s.close()
sys.exit(0)
