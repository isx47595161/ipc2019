# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 27-echo-server-multi
# -----------------------------------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# Echo server multiple-conections
# -----------------------------------------------------------------
import socket, sys, select, os
from subprocess import Popen, PIPE

HOST = ''
PORT = 50006
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())

conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s: #Si es una nova connexió
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else: #Si es una connexió ja establerta
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
                data = data.decode()
                pipeData = Popen(data, stdout=PIPE, stderr=PIPE, shell=True)
                for line in pipeData.stdout:
                    actual.sendall(line)
                for line in pipeData.stderr:
                    actual.sendall(line)
s.close()
sys.exit(0)
