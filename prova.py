# /usr/bin/python
#-*- coding: utf-8-*-
#
#
# -------------------------------------
# Adrià Quintero Lázaro
# @ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Señal 10 diu "Hola radiola"
# Señal 12 diu "Adeu Andreu" i finalitza
# -----------------------------------------
from subprocess import Popen, PIPE
import sys,os,signal, socket
global ordre
ordre="cal"
#Functions
def salut(signum,frame):
    global ordre
    ordre="finger"

def data(signum,frame):
    global ordre
    ordre="date"

signal.signal(signal.SIGUSR1,salut)  #10
signal.signal(signal.SIGUSR2,data)  #12

#Start program
print("Welcome, starting program")
print(f"PPID: {os.getpid()}")
#Create a son
pid=os.fork()
if pid !=0:
    print(f"Parent's pid: {os.getpid()} ({pid})")
    sys.exit(0)

print(f"Son's pid: {os.getpid()} ({pid})")
HOST=''
PORT=50007
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conn, addr=s.accept()
print("Connected by", addr)
pipeData=Popen(ordre,stdout=PIPE)
for line in pipeData.stdout:
    conn.send(line)
conn.close()
sys.exit(0)
