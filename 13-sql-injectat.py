# /usr/bin/python
#-*- coding: utf-8-*-
#
# 13-sql-injectat.py
# Prog consulta
# ---------------------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# ---------------------------------------------------
from subprocess import Popen,PIPE,sys
sentencia = sys.argv[1]
consulta = [f"psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"{sentencia}\""]
pipeData = Popen(consulta,stdout=PIPE, shell=True)
for line in pipeData.stdout:
        print(line.decode("utf-8"),end="")
exit(0)
#-------------------------------------------------------------------------------
#Solució
# -------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument('sqlStatment', help='Sentència SQL a executar',metavar='sentènciaSQL')
args = parser.parse_args()

cmd = "psql -qtA -F',' -h 172.17.0.2 -U postgres  training"
pipeData = Popen(cmd, shell = True, bufsize=0, universal_newline=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write("select * from oficinas;\n\q\n")

for line in pipeData.stdout:
  print(line,end="")
sys.exit(0)
