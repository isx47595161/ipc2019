# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql-multi.py
# *nota* no tracta el cas de num_clie inexistent
# -------------------------------------
# Adrià Quintero Lázaro
# @ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
#commandLocal = "psql -qtA -F ';' lab_clinic -c 'select * from pacients;'"
#commandRemote = "psql -qtA -h i11 -U postgres -F ';' training -c "select * from oficinas;"
# -------------------------------------------
import sys,os,signal
def myhandler(signum,frame):
    print("Signal handler called with signal: "), signum
    print("hasta luego lucas!")
    sys.exit(1)

def mydeath(signum,frame):
    print("Signal handler called with signal: ", signum)
    print("No em dona la gana de morir :)")
signal.signal(signal.SIGALRM,myhandler)      #14
signal.signal(signal.SIGUSR2,myhandler)      #12
signal.signal(signal.SIGUSR1,mydeath)        #10
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,mydeath)
signal.alarm(60)
print(os.getpid())
while True:
    pass
signal.alarm(0)
sys.exit(0)
