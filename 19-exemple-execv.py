# /usr/bin/python
#-*- coding: utf-8-*-
#
#
# -------------------------------------
# Adrià Quintero Lázaro
# @ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,os,signal

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    print("Programa Pare", os.getpid(),pid)
    sys.exit(0)

print("Programa Fill", os.getpid(),pid)
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-ls","/")
#os.execlp("ls","ls","-ls","/")
os.execle("/usr/bin/bash","/usr/bin/bash","/var/tmp/M06/ipc2019/show.sh",{"nom":"pep","edat":"25"})
os.execv("/usr/bin/bash",["/usr/bin/bash","/var/tmp/M06/ipc2019/show.sh"])
os.execve("/usr/bin/echo",["/usr/bin/echo","$HOSTNAME","$edat"],{"nom":"joan","edat":"25"})
os.execve("/usr/bin/bash",["/usr/bin/bash","/var/tmp/M06/ipc2019/show.sh"],{"nom":"joan","edat":"25"})
print("hasta luego Lucas!")
sys.exit(0)
