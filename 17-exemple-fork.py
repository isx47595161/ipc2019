# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql-multi.py
# *nota* no tracta el cas de num_clie inexistent
# -------------------------------------
# Adrià Quintero Lázaro
# @ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    os.wait()
    print("Programa Pare", os.getpid(),pid)
else:
    print("Programa Fill", os.getpid(),pid)

print("hasta luego Lucas!")
sys.exit(0)
