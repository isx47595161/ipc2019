# Automatització de tasques

 - Exercicis de repàs de python

 - IPC

## @ edt ASIX M06 2019-2020

Podeu trobar els programes al GitLab de [edtasixm06](https://gitlab.com/edtasixm06/ipc-2019-20.git
)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona


#### Repàs Python / Argsparse / Objectes

**01-head.py [file]**
  
  Mostrar les deu primeres línies de file o stdin

**02-exemple-args.py**

  Exemple fet interactivament per mostrar el funcionament
  de argparser. Pyton documentation 15.4.

 *Argparser (Python documentation 14.5 / argparse tutorial):*
   * creació objecte. afegir arguments.
   * paràmetres posicionals i opcionals.
   * help, description, epíleg, prog, metavar.
   * destination, type.
   * fer el parse. diccionari de resultats.
   * fitxers de tipus file (inconvenients) o str.
   * validació automàtica i missatges d’error. Independència de l’ordre dels 
     arguments opcionals (no dels posicionals).
   * validació automàtica del tipus dels arguments.
   * Els *paràmetres posicionals* es defineixen per ordre i simplement amb el
      “nom”. Els *opcionals* poden anar en qualsevol ordre i cal definir “-n” i “--nom”.


