# /usr/bin/python
#-*- coding: utf-8-*-
#
#
# -------------------------------------
# Adrià Quintero Lázaro
# @ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Señal 10 diu "Hola radiola"
# Señal 12 diu "Adeu Andreu" i finalitza
# -----------------------------------------
import sys,os,signal

def hola(signum,frame):
    print("Signal handler called with signal:",signum)
    print("Hola radiola")

def adeu(signum,frame):
    print("Signal handler called with signal:",signum)
    print("Adeu Andreu")
    sys.exit(1)

signal.signal(signal.SIGUSR1,hola)  #10
signal.signal(signal.SIGUSR2,adeu)  #12

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
    os.wait()
    print("Programa Pare", os.getpid(),pid)
else:
    print("Programa Fill", os.getpid(),pid)
    while True:
        pass

print("hasta luego Lucas!")
sys.exit(0)
