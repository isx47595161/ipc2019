# /usr/bin/python3
#-*- coding: utf-8-*-
# 26-telnet-client.py
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
    command = str(input("Entra una ordre: "))
    if not command: break
    s.sendall(bytes(command,'utf-8'))
    #pipeData = Popen(command,shell=True,stdout=PIPE)
    while True:
        data=s.recv(1024)
        if data[-1:]=="EOT":
            print('Rebut: ',repr(data[:-1]))
            break
        print(data)
s.close()
sys.exit(0)
