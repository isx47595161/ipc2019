#!/usr/bin/python3
# -*- coding: utf-8 -*-
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
#head [file]
#Programa que mostra les 10 primeres línies de la entrada estàndar o d'un fitxer
# -------------------------------------
#Imports
import sys

#Variables
MAXLIN = 10

fileIn=sys.stdin

#Llegir fitxer
if len(sys.argv) == 2:
    fileIn=open(sys.argv[1],"r")
counter = 0
for line in fileIn:
    counter += 1
    print(line,end="")
    if counter == MAXLIN: break
fileIn.close()
exit(0)
